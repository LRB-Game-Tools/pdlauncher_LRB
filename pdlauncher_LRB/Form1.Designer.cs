﻿namespace pdlauncher_LRB
{
    partial class launcherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ipAddrBox = new System.Windows.Forms.TextBox();
            this.ipLabel = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.launchButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pathBox = new System.Windows.Forms.TextBox();
            this.pathLabel = new System.Windows.Forms.Label();
            this.resLabel = new System.Windows.Forms.Label();
            this.resList = new System.Windows.Forms.ComboBox();
            this.langLabel = new System.Windows.Forms.Label();
            this.langList = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.portBox = new System.Windows.Forms.TextBox();
            this.serverQueryLabel = new System.Windows.Forms.Label();
            this.toDefaultButton = new System.Windows.Forms.Button();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.portLabel = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.topPanel = new System.Windows.Forms.Panel();
            this.exitButton = new System.Windows.Forms.Button();
            this.settingsPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ipAddrBox
            // 
            this.ipAddrBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.ipAddrBox.ForeColor = System.Drawing.Color.Black;
            this.ipAddrBox.Location = new System.Drawing.Point(3, 38);
            this.ipAddrBox.Name = "ipAddrBox";
            this.ipAddrBox.Size = new System.Drawing.Size(214, 27);
            this.ipAddrBox.TabIndex = 0;
            this.ipAddrBox.Text = "173.248.136.133";
            this.toolTip1.SetToolTip(this.ipAddrBox, "The IP address of the Login Server you would like to use");
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.BackColor = System.Drawing.Color.Transparent;
            this.ipLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.ipLabel.ForeColor = System.Drawing.Color.Khaki;
            this.ipLabel.Location = new System.Drawing.Point(4, 15);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(77, 20);
            this.ipLabel.TabIndex = 1;
            this.ipLabel.Text = "IP Address:";
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Segoe UI Light", 14F);
            this.saveButton.ForeColor = System.Drawing.Color.Khaki;
            this.saveButton.Location = new System.Drawing.Point(3, 362);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(214, 37);
            this.saveButton.TabIndex = 2;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // launchButton
            // 
            this.launchButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(110)))), ((int)(((byte)(174)))));
            this.launchButton.FlatAppearance.BorderSize = 0;
            this.launchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.launchButton.Font = new System.Drawing.Font("Segoe UI Light", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.launchButton.ForeColor = System.Drawing.Color.Khaki;
            this.launchButton.Location = new System.Drawing.Point(810, 455);
            this.launchButton.Name = "launchButton";
            this.launchButton.Size = new System.Drawing.Size(197, 55);
            this.launchButton.TabIndex = 3;
            this.launchButton.Text = "Launch";
            this.launchButton.UseVisualStyleBackColor = false;
            this.launchButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(110)))), ((int)(((byte)(174)))));
            this.refreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.refreshButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(128)))), ((int)(((byte)(177)))));
            this.refreshButton.FlatAppearance.BorderSize = 0;
            this.refreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refreshButton.Font = new System.Drawing.Font("Segoe UI Light", 14F);
            this.refreshButton.ForeColor = System.Drawing.Color.Khaki;
            this.refreshButton.Location = new System.Drawing.Point(20, 469);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(118, 38);
            this.refreshButton.TabIndex = 4;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = false;
            this.refreshButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pathBox
            // 
            this.pathBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.pathBox.ForeColor = System.Drawing.Color.Black;
            this.pathBox.Location = new System.Drawing.Point(3, 329);
            this.pathBox.Name = "pathBox";
            this.pathBox.Size = new System.Drawing.Size(214, 27);
            this.pathBox.TabIndex = 6;
            this.pathBox.Tag = "";
            this.pathBox.Text = "C:\\Program Files\\Maxis\\The Sims Online\\TSOClient";
            this.toolTip1.SetToolTip(this.pathBox, "The path where TSO / Project Dollhouse is installed");
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.BackColor = System.Drawing.Color.Transparent;
            this.pathLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.pathLabel.ForeColor = System.Drawing.Color.Khaki;
            this.pathLabel.Location = new System.Drawing.Point(4, 306);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(152, 20);
            this.pathLabel.TabIndex = 7;
            this.pathLabel.Text = "Project Dollhouse path:";
            // 
            // resLabel
            // 
            this.resLabel.AutoSize = true;
            this.resLabel.BackColor = System.Drawing.Color.Transparent;
            this.resLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.resLabel.ForeColor = System.Drawing.Color.Khaki;
            this.resLabel.Location = new System.Drawing.Point(4, 199);
            this.resLabel.Name = "resLabel";
            this.resLabel.Size = new System.Drawing.Size(76, 20);
            this.resLabel.TabIndex = 8;
            this.resLabel.Text = "Resolution:";
            // 
            // resList
            // 
            this.resList.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.resList.ForeColor = System.Drawing.Color.Black;
            this.resList.FormattingEnabled = true;
            this.resList.Items.AddRange(new object[] {
            "1920x1080",
            "1280x720",
            "1024x768",
            "800x600",
            "420x420"});
            this.resList.Location = new System.Drawing.Point(3, 222);
            this.resList.Name = "resList";
            this.resList.Size = new System.Drawing.Size(214, 28);
            this.resList.TabIndex = 9;
            this.toolTip1.SetToolTip(this.resList, "4:3 Resolutions are recommended");
            // 
            // langLabel
            // 
            this.langLabel.AutoSize = true;
            this.langLabel.BackColor = System.Drawing.Color.Transparent;
            this.langLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.langLabel.ForeColor = System.Drawing.Color.Khaki;
            this.langLabel.Location = new System.Drawing.Point(4, 252);
            this.langLabel.Name = "langLabel";
            this.langLabel.Size = new System.Drawing.Size(73, 20);
            this.langLabel.TabIndex = 10;
            this.langLabel.Text = "Language:";
            // 
            // langList
            // 
            this.langList.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.langList.ForeColor = System.Drawing.Color.Black;
            this.langList.FormattingEnabled = true;
            this.langList.Items.AddRange(new object[] {
            "English",
            "Finnish",
            "German",
            "Norwegian",
            "Portuguese",
            "Spanish"});
            this.langList.Location = new System.Drawing.Point(3, 275);
            this.langList.Name = "langList";
            this.langList.Size = new System.Drawing.Size(214, 28);
            this.langList.TabIndex = 11;
            this.toolTip1.SetToolTip(this.langList, "Only installed languages can be used");
            // 
            // portBox
            // 
            this.portBox.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.portBox.ForeColor = System.Drawing.Color.Black;
            this.portBox.Location = new System.Drawing.Point(3, 90);
            this.portBox.Name = "portBox";
            this.portBox.Size = new System.Drawing.Size(214, 27);
            this.portBox.TabIndex = 14;
            this.portBox.Tag = "";
            this.toolTip1.SetToolTip(this.portBox, "The path where TSO / Project Dollhouse is installed");
            // 
            // serverQueryLabel
            // 
            this.serverQueryLabel.BackColor = System.Drawing.Color.Transparent;
            this.serverQueryLabel.Font = new System.Drawing.Font("Segoe UI Light", 24F);
            this.serverQueryLabel.ForeColor = System.Drawing.Color.Khaki;
            this.serverQueryLabel.Location = new System.Drawing.Point(12, 54);
            this.serverQueryLabel.Name = "serverQueryLabel";
            this.serverQueryLabel.Size = new System.Drawing.Size(780, 150);
            this.serverQueryLabel.TabIndex = 12;
            this.serverQueryLabel.Text = "City: ??\r\nPlayers currently online:  ??\r\nRunning time: ??\r\n";
            // 
            // toDefaultButton
            // 
            this.toDefaultButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.toDefaultButton.FlatAppearance.BorderSize = 0;
            this.toDefaultButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toDefaultButton.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.toDefaultButton.ForeColor = System.Drawing.Color.Khaki;
            this.toDefaultButton.Location = new System.Drawing.Point(98, 123);
            this.toDefaultButton.Name = "toDefaultButton";
            this.toDefaultButton.Size = new System.Drawing.Size(122, 36);
            this.toDefaultButton.TabIndex = 13;
            this.toDefaultButton.Text = "Default";
            this.toDefaultButton.UseVisualStyleBackColor = false;
            this.toDefaultButton.Click += new System.EventHandler(this.button5_Click);
            // 
            // settingsPanel
            // 
            this.settingsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(164)))));
            this.settingsPanel.Controls.Add(this.portLabel);
            this.settingsPanel.Controls.Add(this.portBox);
            this.settingsPanel.Controls.Add(this.toDefaultButton);
            this.settingsPanel.Controls.Add(this.pathBox);
            this.settingsPanel.Controls.Add(this.pathLabel);
            this.settingsPanel.Controls.Add(this.saveButton);
            this.settingsPanel.Controls.Add(this.langLabel);
            this.settingsPanel.Controls.Add(this.langList);
            this.settingsPanel.Controls.Add(this.ipAddrBox);
            this.settingsPanel.Controls.Add(this.resLabel);
            this.settingsPanel.Controls.Add(this.resList);
            this.settingsPanel.Controls.Add(this.ipLabel);
            this.settingsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.settingsPanel.Location = new System.Drawing.Point(798, 50);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(220, 472);
            this.settingsPanel.TabIndex = 14;
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.BackColor = System.Drawing.Color.Transparent;
            this.portLabel.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.portLabel.ForeColor = System.Drawing.Color.Khaki;
            this.portLabel.Location = new System.Drawing.Point(4, 68);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(37, 20);
            this.portLabel.TabIndex = 15;
            this.portLabel.Text = "Port:";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(164)))));
            this.topPanel.Controls.Add(this.exitButton);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1018, 50);
            this.topPanel.TabIndex = 15;
            this.topPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.topPanel.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            this.topPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.topPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(120)))), ((int)(((byte)(174)))));
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.ForeColor = System.Drawing.Color.Khaki;
            this.exitButton.Location = new System.Drawing.Point(927, 3);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(88, 41);
            this.exitButton.TabIndex = 16;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // launcherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::pdlauncher_LRB.Properties.Resources.ej;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1018, 522);
            this.Controls.Add(this.launchButton);
            this.Controls.Add(this.serverQueryLabel);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.settingsPanel);
            this.Controls.Add(this.topPanel);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Khaki;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "launcherForm";
            this.Text = "LetsRaceBwoi\'s Project Dollhouse Launcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox ipAddrBox;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button launchButton;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox pathBox;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.Label resLabel;
        private System.Windows.Forms.ComboBox resList;
        private System.Windows.Forms.Label langLabel;
        private System.Windows.Forms.ComboBox langList;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label serverQueryLabel;
        private System.Windows.Forms.Button toDefaultButton;
        private System.Windows.Forms.Panel settingsPanel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.TextBox portBox;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Button exitButton;
    }
}

