﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Management;
using System.Diagnostics;
using System.Windows.Forms;

namespace pdlauncher_LRB
{
    public partial class launcherForm : Form
    {
        int scrollspeed = 10;
        bool mousedown = false;
        int msx = 0;
        int msy = 0;
        System.Resources.ResourceManager resources;
        public launcherForm()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Folder containing Project Dollhouse";
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "";
            openFileDialog1.Multiselect = true;
            openFileDialog1.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
            string[] lines = File.ReadAllLines(pathBox.Text + @"\Project Dollhouse Client.exe.config");
            ipAddrBox.Text = lines[73].Replace("                <value>", "").Replace("</value>", "");
            portBox.Text = lines[16].Replace("                <value>", "").Replace("</value>", "");                     
            resList.Text = lines[22].Replace("                <value>", "").Replace("</value>", "") + "x" + lines[25].Replace("                <value>", "").Replace("</value>", "");           
            langList.Text = lines[13].Replace("                <value>", "").Replace("</value>", "");
            getStream();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            setStream();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process process1 = new Process();
            process1.StartInfo.FileName = (pathBox.Text + @"\PDPatcher.exe");
            process1.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ipAddrBox.Text = "173.248.136.133";
            portBox.Text = "2106";
            button1_Click(toDefaultButton, null);
        }

        /// <summary>
        /// Get the server status
        /// </summary>
        private void getStream()
        {
            try
            {
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create("http://" + ipAddrBox.Text + ":8888/city");
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);
                string sLine = "";
                serverQueryLabel.Text = objReader.ReadToEnd().Replace("<b>", "").Replace("</b>", "").Replace("</br>", Environment.NewLine).Replace(@"<a href=""http://nancyfx.org"">", "").Replace("</a>", "");
            }
            catch
            {
                serverQueryLabel.Text = "Error getting server status." + Environment.NewLine + "Does the server exist?";

            }
            if (serverQueryLabel.Text.Contains("City: East Jerome"))
            {
                System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(launcherForm));
                this.BackgroundImage = new Bitmap(pdlauncher_LRB.Properties.Resources.ej);

            }
            else
            {
                System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(launcherForm));
                this.BackgroundImage = new Bitmap(pdlauncher_LRB.Properties.Resources.screenshottest);
            }
        }

        /// <summary>
        /// Set the program configuration
        /// </summary>
        private void setStream()
        {
           try
           {
                string[] lines = File.ReadAllLines(pathBox.Text + @"\Project Dollhouse Client.exe.config");
                lines[73] = "                <value>" + ipAddrBox.Text + "</value>";
                lines[16] = "                <value>" + portBox.Text + "</value>";
                lines[22] = "                <value>" + resList.Text.Remove(resList.Text.IndexOf("x"), resList.Text.Length - resList.Text.IndexOf("x")) + "</value>";
                lines[25] = "                <value>" + resList.Text.Remove(0, resList.Text.Length - resList.Text.IndexOf("x")).Replace("x", "") + "</value>";
                lines[13] = "                <value>" + langList.Text + "</value>";
                File.WriteAllLines(pathBox.Text + @"\Project Dollhouse Client.exe.config", lines);
                try
                {
                    WebRequest wrGETURL;
                    wrGETURL = WebRequest.Create("http://" + ipAddrBox.Text + ":8888/city");
                    Stream objStream;
                    objStream = wrGETURL.GetResponse().GetResponseStream();
                    StreamReader objReader = new StreamReader(objStream);
                    serverQueryLabel.Text = objReader.ReadToEnd().Replace("<b>", "").Replace("</b>", "").Replace("</br>", Environment.NewLine).Replace(@"<a href=""http://nancyfx.org"">", "").Replace("</a>", "");
                }
                catch
                {
                    serverQueryLabel.Text = "Error getting server status." + Environment.NewLine + "Does the server exist?";

                }
            }
            catch 
            {
                MessageBox.Show("There was a problem doing that! Do you have the right path?");
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            getStream();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Cursor.Position.X - this.Location.X < 700)
            {
                while (settingsPanel.Width > 10)
                {
                    settingsPanel.Width = settingsPanel.Width - scrollspeed;
                    if (settingsPanel.Width < 75)
                    {
                        if (scrollspeed > 2)
                        {
                            scrollspeed = scrollspeed - 1;
                        }

                    }
                    this.Refresh();
                    settingsPanel.Refresh();
                    System.Threading.Thread.Sleep(1);
                }
                settingsPanel.BackColor = Color.FromArgb(50, settingsPanel.BackColor);
                scrollspeed = 10;
            }
            else
            {
                settingsPanel.BackColor = Color.FromArgb(255, settingsPanel.BackColor);
                while (settingsPanel.Width != 220)
                {
                    settingsPanel.Width = settingsPanel.Width + 10;
                    this.Refresh();
                    settingsPanel.Refresh();
                    System.Threading.Thread.Sleep(1);
                }
                scrollspeed = 10;
            }

        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mousedown = true;
            msx = e.X;
            msy = e.Y;
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mousedown = false;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mousedown)
            {
                this.Location = new Point(this.Location.X + (e.X - msx), this.Location.Y + (e.Y - msy));
            }
        }

        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            mousedown = false;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
