# pdlauncher_LRB
A custom launcher written in C# for Project Dollhouse, modified for use with The Sims 2 Online.

## Features
* Launches the game
* Settings changes in a single click
* Updating (WIP)
* City server status
* Custom UI
* Animations
